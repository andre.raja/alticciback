package raja.andre.alticci.controllers;

public interface AlticciController {

    int getAlticciValue(int n);
}
