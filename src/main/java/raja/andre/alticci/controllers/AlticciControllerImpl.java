package raja.andre.alticci.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import raja.andre.alticci.services.AlticciService;

@RestController
@RequestMapping("/alticci")
public class AlticciControllerImpl implements AlticciController {

    @Autowired
    private AlticciService alticciService;

    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/{n}")
    public int getAlticciValue(@PathVariable int n) {
        return alticciService.getAlticciValue(n);
    }
}
