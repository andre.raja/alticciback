package raja.andre.alticci.services;

public interface AlticciService {

    int getAlticciValue(int n);
}
