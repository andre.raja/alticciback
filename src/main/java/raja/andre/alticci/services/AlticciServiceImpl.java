package raja.andre.alticci.services;

import org.springframework.stereotype.Service;

@Service
public class AlticciServiceImpl implements AlticciService {
    @Override
    public int getAlticciValue(int n) {
        if (n < 2) return n;
        if (n == 2) return n-1;
        return getAlticciValue(n-3) + getAlticciValue(n-2);
    }
}
