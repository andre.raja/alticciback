package raja.andre.alticci.services;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AlticciServiceImplTest {

    private AlticciService alticciService = new AlticciServiceImpl();

    @Test
    void getAlticciValue() {
        assertEquals(0, alticciService.getAlticciValue(0));
        assertEquals(1, alticciService.getAlticciValue(1));
        assertEquals(1, alticciService.getAlticciValue(2));
        assertEquals(1, alticciService.getAlticciValue(3));
        assertEquals(9, alticciService.getAlticciValue(10));
        assertEquals(37, alticciService.getAlticciValue(15));
    }
}
